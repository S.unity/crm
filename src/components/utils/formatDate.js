export default function formattedDate (data, isHours = false) {
  let x = new Date(data)
  x = (x.getDate() <= 9 ? '0' + x.getDate() : x.getDate())
        + '/' + ((x.getMonth() <= 9 && x.getMonth() + 1 <= 9) ? '0' + (x.getMonth() + 1) : (x.getMonth() + 1))
        + '/' + x.getFullYear()
        + ( isHours ? ` ${(x.getHours() <= 9 ? '0' + x.getHours() : (x.getHours()))}:${(x.getMinutes() <= 9 ? '0' + x.getMinutes() : x.getMinutes())}` : '')
  return x
}
