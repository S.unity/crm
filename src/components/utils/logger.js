import api from '@/API/api'
import store from '@/vuex/store'

export default async function logger (lead_id, log) {
  const user = {username: store.state.user.username, _id: store.state.user._id}
  return await api.logLead({
    lead_id,
    user,
    log: log.length && log || ''
  })
}
