import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home'
import Deals from '@/components/Deals'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Deals,
    meta: {requiresAuth: true}
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import(/* webpackChunkName: "about" */ '../components/Admin'),
  },
  {
    path: '/lead/:id',
    name: 'Lead',
    component: () => import(/* webpackChunkName: "about" */ '../components/Lead'),
    props: true,
    meta: {requiresAuth: true}
  },
  {
    path: '/deal',
    name: 'Deal',
    component: () => import(/* webpackChunkName: "about" */ '../components/Deals'),
    meta: {requiresAuth: true}
  },
  {
    path: '/analytics',
    name: 'Analytics',
    component: () => import(/* webpackChunkName: "about" */ '../components/Analytics'),
  },
  {
    path: '/articles',
    name: 'Articles',
    component: () => import(/* webpackChunkName: "about" */ '../components/lists/Articles'),
  },
  {
    path: '/temp-docs',
    name: 'tempDocs',
    component: () => import(/* webpackChunkName: "about" */ '../components/lists/tempDocs'),
  },
  {
    path: '/documents',
    name: 'Documents',
    component: () => import(/* webpackChunkName: "about" */ '../components/Documents'),
  },
  {
    path: '/edit',
    name: 'Edit',
    component: () => import(/* webpackChunkName: "about" */ '../components/Edit'),
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "about" */ '../components/Settings'),
    meta: {requiresAuth: true}
  },
  {
    path: '/tasks',
    name: 'Tasks',
    component: () => import(/* webpackChunkName: "about" */ '../components/Tasks'),
    meta: {requiresAuth: true}
  },
  {
    path: '/auth',
    name: 'Auth',
    component: () => import(/* webpackChunkName: "about" */ '../components/Auth'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
