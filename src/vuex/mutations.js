export default {
  REGISTRATION: (state, user) => {
    if (user.errorToFields) {
      state.auth.error = user.errorToFields
      setTimeout(() => {
        state.auth.error = []
      }, 1000)
    } else {
      state.auth.error = []
      state.auth.token = user.token
      state.auth.email = user.email
      state.auth.admin = false
      state.auth.sto = user.sto
    }
  },
  setUserData(state, data) {
    state.user = data.user
    state.token = data.token
    state.error = null
    localStorage.setItem('user', JSON.stringify(state.user));
    localStorage.setItem('token', state.token);
  },
  GO_OUT: (state) => {
    state.user = {}
    state.token = null
  },
}
