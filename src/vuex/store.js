import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
// import getters from './getters/getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: JSON.parse(localStorage.getItem('user')) || {},
    token: localStorage.getItem('token') || null,
    error: [],
  },
  mutations,
  getters: {
    AUTH (state) {
      return !!state.token
    },
    USER (state) {
      return state.user
    },
    ERROR (state) {
      return state.error
    }
  },
  actions
})
export default store
