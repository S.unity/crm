import api from '@/API/api'

export default {
  async GO_OUT ({ commit }) {
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    localStorage.removeItem('columns')
    commit('GO_OUT')
  },
  async LOGIN ({ commit }, request) {
    const {data} = await api.login(request)
    commit('setUserData', data)
    return data
  },
}
