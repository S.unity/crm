import axios from 'axios'
export default () => {
  return axios.create({
    // baseURL: 'http://192.168.2.67:5000/'
    // baseURL: 'http://127.0.0.1:5000/'
    baseURL: 'https://technik-crm.ru:5000/'
  })
}
