import apiAddress from "./address";
import store from '../vuex/store'
import axios from 'axios'

export default {
  registration (params) {
    return apiAddress().post('registration', params)
  },
  login (params) {
    return apiAddress().post('auth/login', params)
  },
  application (params) {
    return apiAddress().post('new-application', params, {
      headers: {
        // eslint-disable-next-line
        authorization: store.state.user.token
      }
    })
  },
  updateProfile (params) {
    return apiAddress().post('update-profile', params, {
      headers: {
        // eslint-disable-next-line
        authorization: store.state.user.token
      }
    })
  },
  addToOffer (params) {
    return apiAddress().post('add-offer', params, {
      headers: {
        // eslint-disable-next-line
        authorization: store.state.token
      }
    })
  },

  getUsers () {
    return apiAddress().get('auth/users', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  getAdmins () {
    return apiAddress().get('auth/get-admins', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  changeUser (data) {
    return apiAddress().put('auth/change-user', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  addUser (data) {
    return apiAddress().post('auth/registration', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  deleteUser (data) {
    return apiAddress().delete('auth/delete-user', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },

  addLead (data) {
    return apiAddress().post('lead/import-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  searchLead (data) {
    return apiAddress().post('lead/search-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  distinctLead (data) {
    return apiAddress().post('lead/distinct-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  sortLead (data) {
    return apiAddress().post('lead/sort-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getLeads (data) {
    return apiAddress().post('lead/leads', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  settingsLead (data) {
    return apiAddress().post('lead/settings-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  getBasket (data) {
    return apiAddress().post('lead/basket', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  getCountLeadInBasket () {
    return apiAddress().get('lead/count-basket', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  getTasksLead (data) {
    return apiAddress().post('lead/get-tasks', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getLead (data) {
    return apiAddress().post('lead/lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  changeLeadFields (data) {
    return apiAddress().post('lead/lead-change-fields', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  changeLead (data) {
    return apiAddress().post('lead/lead-change', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  changeLeadTask (data) {
    return apiAddress().post('lead/lead-change-task', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  deleteLead (data) {
    return apiAddress().delete('lead/delete-lead', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },
  toBasketLead (data) {
    return apiAddress().post('lead/to-basket-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`,
      },
    })
  },

  selectResponsibleLead (data) {
    return apiAddress().put('lead/select-responsible-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  sendMessage (data) {
    return apiAddress().post('lead/send-message-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`,
      },
    })
  },
  sendFile (data) {
    return apiAddress().post('lead/send-file-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`,
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  executeTaskLead (data) {
    return apiAddress().put('lead/execute-task-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },

  getStatusLead () {
    return apiAddress().get('other/get-status-leads', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  selectStatusLead (data) {
    return apiAddress().put('other/select-status-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  swapTaskResponsible (data) {
    return apiAddress().put('lead/swap-task-responsible', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeStatusLead (data) {
    return apiAddress().put('other/change-status-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  updateLead (data) {
    return apiAddress().post('lead/update-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      }
    })
  },
  deleteStatusLead (data) {
    return apiAddress().delete('other/delete-status-lead', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },
  addStatusLead (data) {
    return apiAddress().post('other/add-status-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getStatusLeads () {
    return apiAddress().get('other/get-status-leads', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },


  addLeadInfo (data) {
    return apiAddress().post('other/add-lead-info', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getLeadInfo () {
    return apiAddress().get('other/get-lead-info', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeLeadInfo (data) {
    return apiAddress().put('other/change-lead-info', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  deleteLeadInfo (data) {
    return apiAddress().delete('other/delete-lead-info', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },

  addRegion (data) {
    return apiAddress().post('other/add-region', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getRegions () {
    return apiAddress().get('other/get-regions', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeRegion (data) {
    // return apiAddress().post('mail.py', data)
    return apiAddress().put('lead/change-region', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeRegionSettings (data) {
    // return apiAddress().post('mail.py', data)
    return apiAddress().put('other/change-region', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },

  addTask (data) {
    return apiAddress().post('other/add-task', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getTasks () {
    return apiAddress().get('other/get-tasks', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeTask (data) {
    return apiAddress().put('other/change-task', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  deleteTask (data) {
    return apiAddress().delete('other/delete-task', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },

  addFields (data) {
    return apiAddress().post('other/add-fields', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getFields () {
    return apiAddress().get('other/get-fields', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeFields (data) {
    return apiAddress().put('other/change-fields', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  deleteFields (data) {
    return apiAddress().delete('other/delete-fields', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },

  addTemplateSMS (data) {
    return apiAddress().post('other/add-template-sms', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getTemplateSMS () {
    return apiAddress().get('other/get-template-sms', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  changeTemplateSMS (data) {
    return apiAddress().put('other/change-template-sms', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  deleteTemplateSMS (data) {
    return apiAddress().delete('other/delete-template-sms', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data: data
    })
  },

  getRoles () {
    return apiAddress().get('other/get-roles', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getDocuments () {
    return apiAddress().get('other/get-documents', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  deleteDocument (data) {
    return apiAddress().delete('/delete-file', {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
      data
    })
  },
  logLead (data) {
    return apiAddress().post('lead/log-lead', data, {
      headers: {
        // eslint-disable-next-line
        'Authorization': `Bearer ${store.state.token}`
      },
    })
  },
  getCities () {
    return apiAddress().get('other/get-cities')
  },
}
