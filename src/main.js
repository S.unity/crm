import Vue, { onMounted } from 'vue'
import App from './App.vue'
import store from './vuex/store'
import router from './router'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.token) {
      next()
      return
    }
    next('/auth')
  } else {
    next()
  }
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
